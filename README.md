# Codes and data for reviewing the results presented in the submitted paper.

## Repository structure

Each folder _sectionX_ represents the sections of the submitted article, containing several .Rmd files in order to reproduce the displayed results (from data generation/import to function calls).

The _data/_ folder in each section folder gathers the different exported results (data frames, objects...) after the Shapley indices have been computed (since the algorithm may take a long time).

The _customFunctions/_ folder aims at gathering custom made functions (i.e., not implemented in a package) which are imported when convenient in each .Rmd file.

## Software versions

The following versions of the different software/packages used are the following:

### Software

- R : 4.0.3
- RStudio : 1.3.1093

### Packages

- sensitivity : 1.23.1
- mvtnorm : 1.1.1
- corrplot : 0.84
- RANN : 2.6.1
- cubature : 2.0.4.1
- parallel : 4.0.2
- lattice : 0.20.41
- rlang : 0.4.9
- RColorBrewer : 1.1.2
